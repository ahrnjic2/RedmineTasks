package com.aporiadev.ahrnjic.redminetasks.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.databinding.ItemAttachmentBinding;
import com.aporiadev.ahrnjic.redminetasks.model.Attachment;
import com.aporiadev.ahrnjic.redminetasks.viewmodel.AttachmentViewModel;

import java.util.List;

/**
 * Created by ahrnjic on 28/09/17.
 */

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.BindingHolder> {

    private static final String TAG = "LOG_" + AttachmentAdapter.class.getSimpleName();

    private List<Attachment> attachments;

    public AttachmentAdapter(List<Attachment> attachments) {
        Log.i(TAG, "AttachmentAdapter: contains " + attachments.size() + " attachments");
        this.attachments = attachments;
    }

    @Override
    public AttachmentAdapter.BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemAttachmentBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_attachment,
                parent,
                false);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ItemAttachmentBinding binding = holder.binding;
        binding.setAttachment(new AttachmentViewModel(attachments.get(position)));
    }

    @Override
    public int getItemCount() {
        return attachments.size();
    }

    @Override
    public long getItemId(int position) {
        return attachments.get(position).getId();
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemAttachmentBinding binding;

        BindingHolder(ItemAttachmentBinding binding) {
            super(binding.attachmentMainLayout);
            this.binding = binding;
        }
    }
}
