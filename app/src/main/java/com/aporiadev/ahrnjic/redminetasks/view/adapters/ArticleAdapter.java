package com.aporiadev.ahrnjic.redminetasks.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.databinding.ItemArticleBinding;
import com.aporiadev.ahrnjic.redminetasks.model.Article;
import com.aporiadev.ahrnjic.redminetasks.viewmodel.ArticleViewModel;

import java.util.List;

/**
 * Created by ahrnjic on 18/09/17.
 */

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.BindingHolder> {

    private static final String TAG = "LOG_" + ArticleAdapter.class.getSimpleName();

    private List<Article> mArticles;

    public ArticleAdapter(List<Article> mArticles) {
        this.mArticles = mArticles;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemArticleBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_article, parent, false);

        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, final int position) {
        ItemArticleBinding binding = holder.binding;
        binding.setAvm(new ArticleViewModel(mArticles.get(position)));
    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }

    @Override
    public long getItemId(int position) {
        return mArticles.get(position).getId();
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemArticleBinding binding;

        BindingHolder(ItemArticleBinding binding) {
            super(binding.mainLayout);
            this.binding = binding;
        }


    }

}
