package com.aporiadev.ahrnjic.redminetasks.view.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.aporiadev.ahrnjic.redminetasks.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_AUTH_BASE_64;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsString;

/**
 * Created by ahrnjic on 21/10/17.
 */

public class AuthDialogFragment extends DialogFragment {
    private static final String TAG = "LOG_" + AuthDialogFragment.class.getSimpleName();

    private ICallback callback;

    @BindView(R.id.auth_user)
    EditText et_user;
    @BindView(R.id.auth_pass)
    EditText et_pass;

    public AuthDialogFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        Log.i(TAG, "onAttach: ");
        super.onAttach(context);
        if (context instanceof AuthDialogFragment.ICallback) {
            callback = (AuthDialogFragment.ICallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement AuthDialogFragment.ICallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @Override
    public void onResume() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        getDialog().getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        super.onResume();
    }

    @OnClick(R.id.auth_ok)
    void onAuthOkClick() {
        Log.i(TAG, "onAuthOkClick: ");
        // TODO: 27/09/17  Check if the link provided is a correct rss/atom link

        String auth = et_user.getText() + ":" + et_pass.getText();
        String basicAuth = "Basic " + Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        putPrefsString(getActivity(), KEY_AUTH_BASE_64, basicAuth);

        callback.proceedDownload();
        dismiss();
    }

    public interface ICallback {
        void proceedDownload();
    }
}
