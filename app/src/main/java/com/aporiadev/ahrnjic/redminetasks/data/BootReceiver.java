package com.aporiadev.ahrnjic.redminetasks.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import static com.aporiadev.ahrnjic.redminetasks.utils.AlarmUtils.startAlarm;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_ALARM_EXISTS;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsBoolean;

/**
 * Created by ahrnjic on 19/09/17.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if (getPrefsBoolean(context, KEY_ALARM_EXISTS, false)) {
            startAlarm(context);
            Toast.makeText(context, "RedmineTasks started.", Toast.LENGTH_SHORT).show();
        }
    }
}
