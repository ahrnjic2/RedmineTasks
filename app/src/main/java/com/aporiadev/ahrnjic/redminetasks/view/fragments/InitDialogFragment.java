package com.aporiadev.ahrnjic.redminetasks.view.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.aporiadev.ahrnjic.redminetasks.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_BASE_URL;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_FIRST_LAUNCH;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsString;

public class InitDialogFragment extends DialogFragment {
    private static final String TAG = "LOG_" + InitDialogFragment.class.getSimpleName();

    @BindView(R.id.launch_url)
    EditText et_url;
    @BindView(R.id.launch_start)
    Button button_start;

    private ICallback callback;

    public InitDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_init, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.launch_start)
    void onStartClick() {
        Log.i(TAG, "startMainActivity: ");

        // TODO: 27/09/17  Check if the link provided is a correct rss/atom link
        putPrefsBoolean(getActivity(), KEY_FIRST_LAUNCH, false);
        putPrefsString(getActivity(), KEY_BASE_URL, et_url.getText().toString());

        callback.pullAndStartAlarm();
        dismiss();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ICallback) {
            callback = (ICallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement InitDialogFragment.ICallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    @Override
    public void onResume() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        getDialog().getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        super.onResume();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface ICallback {
        void pullAndStartAlarm();
    }
}
