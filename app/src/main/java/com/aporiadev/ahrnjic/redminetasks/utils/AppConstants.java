package com.aporiadev.ahrnjic.redminetasks.utils;

import android.os.Environment;

/**
 * Created by ahrnjic on 26/09/17.
 */

public class AppConstants {
    public static final String PREFS = "REDMINE_TASKS_PREFS";

    public static final String KEY_FIRST_LAUNCH = "FIRST_LAUNCH";
    public static final String KEY_BASE_URL = "BASE_URL";
    public static final String KEY_APP_IN_FOREGROUND = "APP_IN_FOREGROUND";
    public static final String KEY_ALARM_EXISTS = "ALARM_EXISTS";
    public static final String KEY_LAST_UPDATED = "LAST_UPDATED";
    public static final String KEY_AUTH_BASE_64 = "AUTH_BASE_64";
    public static final String KEY_MISSED_UPDATE_NO_CONNECTION = "MISSED_UPDATE_NO_CONNECTION";
    public static final String KEY_CLEAR_ANIMATION_ON_RESUME = "CLEAR_ANIMATION_ON_RESUME";

    public static final String INTENT_FILTER_REFRESH_STARTED = "com.aporiadev.ahrnjic.redminetasks.data.PullService.REFRESH_STARTED";
    public static final String INTENT_FILTER_REFRESH_COMPLETED = "com.aporiadev.ahrnjic.redminetasks.data.PullService.REFRESH_COMPLETED";
    public static final String INTENT_FILTER_PULL_RECEIVER = "com.aporiadev.ahrnjic.redminetasks.data.PullReceiver";
    public static final String INTENT_FILTER_JOB_PULL_SERVICE_COMPLETED = "com.aporiadev.ahrnjic.redminetasks.data.JobPullService.COMPLETED";

    public static final String ATTACHMENTS_BASE_URL = "http://saas1208uq.saas-secure.com";

    public static final int PERMISSION_WRITE_TO_EXTERNAL_STORAGE_REQUEST_CODE = 100;
    public static final String PERMISSION_KEY_WRITE_TO_EXTERNAL_STORAGE_DONT_ASK_AGAIN = "WRITE_TO_EXTERNAL_STORAGE_DONT_ASK_AGAIN";
    public static final int PERMISSION_ACTIVITY_REQ_CODE = 1000;

    public static final String APP_DEFAULT_SAVE_PATH = Environment.getExternalStorageDirectory().toString() + "/redminetasks/files/";

    public static final String GENERIC_FILE_PROVIDER_AUTHORITIES = "com.aporiadev.ahrnjic.redminetasks.utils.FileProvider";

}
