package com.aporiadev.ahrnjic.redminetasks.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by ahrnjic on 26/09/17.
 */

public class ServiceToast implements Runnable {
    private final Context context;
    private String message;

    public ServiceToast(Context context, String message) {
        this.context = context;
        this.message = message;
    }

    public void run() {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
