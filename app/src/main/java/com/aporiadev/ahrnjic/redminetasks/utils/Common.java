package com.aporiadev.ahrnjic.redminetasks.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.aporiadev.ahrnjic.redminetasks.model.Attachment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.Realm;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.APP_DEFAULT_SAVE_PATH;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_AUTH_BASE_64;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsString;

/**
 * Created by ahrnjic on 20/09/17.
 */

public class Common {
    private static final String TAG = "LOG_" + Common.class.getSimpleName();

    @SuppressLint("SimpleDateFormat")
    public static String dateToFormattedString(long date) {
        try {
            Calendar c = Calendar.getInstance(), today = Calendar.getInstance();
            c.setTimeInMillis(date);
            SimpleDateFormat sdf;
            if (c.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                sdf = new SimpleDateFormat("HH:mm");
//                Log.i(TAG, "resolveDate: 1st case: " + sdf.format(c.getTimeInMillis()));
                return sdf.format(c.getTimeInMillis());
            } else {
                sdf = new SimpleDateFormat("MMM dd");
//                Log.i(TAG, "resolveDate: 2nd case: " + sdf.format(c.getTimeInMillis()));
                return sdf.format(c.getTimeInMillis());
            }
        } catch (Exception e) {
            Log.i(TAG, "resolveDate: Exception: " + e.getMessage());
            return String.valueOf(date);
        }
    }

    public static void downloadFileThread(final Context context, final int attachmentId) {
        final String auth = getPrefsString(context, KEY_AUTH_BASE_64, "");
        if (auth.equals("")) {
            Log.i(TAG, "downloadFile: No auth provided - exiting");
            return;
        } else {
            Toast.makeText(context, "Downloading attachments...", Toast.LENGTH_SHORT).show();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "run: new thread for attachment id: " + attachmentId);
                downloadFile(context, attachmentId);
            }
        }).start();
    }

    public static void downloadFile(final Context context, final int attachmentId) {
        Realm.init(context);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    Attachment attachment = realm.where(Attachment.class).equalTo("id", attachmentId).findFirst();
                    long fileSize = -1;
                    int count;
                    URL url = new URL(attachment.getUrl());

                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestProperty("Authorization", getPrefsString(context, KEY_AUTH_BASE_64, ""));
                    con.connect();
                    Log.i(TAG, "downloadFile: responseCode: " + con.getResponseMessage());
                    Log.i(TAG, "downloadFile: contentLength: " + con.getContentLength());

                    // download the taskFile
                    InputStream input = new BufferedInputStream(con.getInputStream());

                    File directory = new File(APP_DEFAULT_SAVE_PATH);
                    directory.mkdirs();

                    File outputFile = new File(directory, attachment.getName());

                    // Output stream
                    OutputStream output = new FileOutputStream(outputFile);

                    byte data[] = new byte[1024];
                    while ((count = input.read(data)) != -1) {
                        fileSize += count;
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();

                    attachment.setSize(fileSize);

                    Log.i(TAG, "downloadFile: fileSize: " + fileSize);

                } catch (IOException e) {
                    Log.i(TAG, "downloadFile: IOException: " + e.toString());
                } catch (Exception e) {
                    Log.i(TAG, "downloadFile: Exception: " + e.toString());
                }
            }
        });
        realm.close();
    }
}
