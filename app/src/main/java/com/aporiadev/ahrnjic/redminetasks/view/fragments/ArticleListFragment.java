package com.aporiadev.ahrnjic.redminetasks.view.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.databinding.FragmentArticleListBinding;
import com.aporiadev.ahrnjic.redminetasks.model.Article;
import com.aporiadev.ahrnjic.redminetasks.view.adapters.ArticleAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.INTENT_FILTER_PULL_RECEIVER;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.INTENT_FILTER_REFRESH_COMPLETED;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.INTENT_FILTER_REFRESH_STARTED;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_CLEAR_ANIMATION_ON_RESUME;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;

/**
 * Created by ahrnjic on 19/09/17.
 */

public class ArticleListFragment extends Fragment {
    private static final String TAG = "LOG_" + ArticleListFragment.class.getSimpleName();
    Receiver refreshAnimationListener = new Receiver();

    @BindView(R.id.article_list)
    RecyclerView rv;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        FragmentArticleListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article_list, container, false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.articleList.setLayoutManager(layoutManager);

        ButterKnife.bind(this, binding.getRoot());

        Realm realm = Realm.getDefaultInstance();
        RealmResults<Article> articles = realm.where(Article.class).findAllSorted("datePublished", Sort.DESCENDING);


        final ArticleAdapter adapter = new ArticleAdapter(articles);
        binding.articleList.setAdapter(adapter);

        RealmChangeListener<RealmResults<Article>> realmChangeListener = new RealmChangeListener<RealmResults<Article>>() {
            @Override
            public void onChange(RealmResults<Article> articles) {
                adapter.notifyDataSetChanged();
            }
        };

        articles.addChangeListener(realmChangeListener);


        swipeRefreshLayout.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getActivity().sendBroadcast(new Intent(INTENT_FILTER_PULL_RECEIVER));
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause: ");
        getActivity().unregisterReceiver(refreshAnimationListener);
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume: ");
        // Check whether animation should be cleared instantly
        if (getPrefsBoolean(getActivity(), KEY_CLEAR_ANIMATION_ON_RESUME, false)) {
            Log.i(TAG, "onResume: clearing animation");
            swipeRefreshLayout.setRefreshing(false);
            putPrefsBoolean(getActivity(), KEY_CLEAR_ANIMATION_ON_RESUME, false);
        }
        // Register broadcast receiver for animation start/stop
        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_FILTER_REFRESH_STARTED);
        filter.addAction(INTENT_FILTER_REFRESH_COMPLETED);
        getActivity().registerReceiver(refreshAnimationListener, filter);
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop: ");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy: ");
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "onDestroyView: ");
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "onDetach: ");
        super.onDetach();
    }

    private class Receiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(INTENT_FILTER_REFRESH_COMPLETED) && swipeRefreshLayout.isRefreshing()) {
                Log.i(TAG, "onReceive: refreshAnimationListener: stop");
                swipeRefreshLayout.setRefreshing(false);
            } else if (intent.getAction().equals(INTENT_FILTER_REFRESH_STARTED) && !swipeRefreshLayout.isRefreshing()) {
                Log.i(TAG, "onReceive: refreshAnimationListener: start");
                swipeRefreshLayout.setRefreshing(true);
            }
        }
    }
}
