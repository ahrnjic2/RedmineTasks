package com.aporiadev.ahrnjic.redminetasks.view.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.databinding.FragmentDetailsBinding;
import com.aporiadev.ahrnjic.redminetasks.model.Article;
import com.aporiadev.ahrnjic.redminetasks.model.Attachment;
import com.aporiadev.ahrnjic.redminetasks.view.activities.PermissionActivity;
import com.aporiadev.ahrnjic.redminetasks.view.adapters.AttachmentAdapter;
import com.aporiadev.ahrnjic.redminetasks.viewmodel.ArticleViewModel;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_AUTH_BASE_64;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.PERMISSION_ACTIVITY_REQ_CODE;
import static com.aporiadev.ahrnjic.redminetasks.utils.Common.downloadFileThread;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.appInForeground;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsString;

/**
 * Created by ahrnjic on 19/09/17.
 */

public class DetailsFragment extends Fragment {
    private static final String TAG = "LOG_" + DetailsFragment.class.getSimpleName();

    Article article;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        FragmentDetailsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);

        // Staggered (grid-like) layout manager for files RecyclerView
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        binding.rvFiles.setLayoutManager(layoutManager);

        // Show 'back' button only on this fragment
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        int id = getArguments().getInt("ID");

        Realm realm = Realm.getDefaultInstance();
        article = realm.where(Article.class).equalTo("id", id).findFirst();
        RealmResults<Attachment> attachments = article.getAttachments().where().notEqualTo("size", -1).findAll();

        if (!article.isOpened()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    article.setOpened(true);
                }
            });
        }

        Log.i(TAG, "onCreateView: article: " + article.getId() + " | " + article.getDatePublished());

        binding.setAvm(new ArticleViewModel(article));

        // Get files for current article
        Log.i(TAG, "onCreateView: article contains " + article.getAttachments().size() + " files");
        for (Attachment a : attachments) {
            Log.i(TAG, "attachment: " + a.getName() + " | " + a.getSize());
        }
        // adapter containing only downloaded files
        final AttachmentAdapter adapter = new AttachmentAdapter(attachments);

        binding.rvFiles.setAdapter(adapter);

        RealmChangeListener<RealmResults<Attachment>> realmChangeListener = new RealmChangeListener<RealmResults<Attachment>>() {
            @Override
            public void onChange(RealmResults<Attachment> a) {
                adapter.notifyDataSetChanged();
            }
        };

        attachments.addChangeListener(realmChangeListener);

        // Download files
        if (article.getAttachments().where().equalTo("size", -1).findAll().size() > 0) {
            if (hasWriteExternalStoragePermission()) {
                Log.i(TAG, "onCreateView: downloadFiles: hasWriteExternalStoragePermission already");
                downloadArticleAttachments();
            }
        }

        realm.close();
        return binding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: requestCode: " + requestCode + " | resultCode: " + resultCode);
        if (requestCode == PERMISSION_ACTIVITY_REQ_CODE && resultCode == Activity.RESULT_OK) {
            Log.i(TAG, "onActivityResult: ");
            downloadArticleAttachments();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void downloadArticleAttachments() {
        Log.i(TAG, "downloadArticleAttachments: ");
        // Request auth credentials
        if (getPrefsString(getActivity(), KEY_AUTH_BASE_64, "").equals("")) {
            AuthDialogFragment authDialogFragment = new AuthDialogFragment();
            authDialogFragment.show(getFragmentManager(), "GetAuthCredentials");
        }
        for (Attachment attachment : article.getAttachments().where().equalTo("size", -1).findAll()) {
            Log.i(TAG, "downloadArticleAttachments: downloading: " + attachment.getUrl());
            downloadFileThread(getActivity(), attachment.getId());
        }
    }

    @TargetApi(23)
    private boolean hasWriteExternalStoragePermission() {
        if (Build.VERSION.SDK_INT <= 21)
            return true;
        // Check if write external storage permission is already granted
        if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else if (appInForeground(getActivity())) {
            startActivityForResult(new Intent(getActivity(), PermissionActivity.class), PERMISSION_ACTIVITY_REQ_CODE);
        } else {
            // TODO: 27/09/17 Send notification about missing permission
        }
        return false;
    }

    public void proceedDownload() {
        Log.i(TAG, "proceedDownload: ");
        downloadArticleAttachments();
    }
}
