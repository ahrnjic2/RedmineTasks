package com.aporiadev.ahrnjic.redminetasks.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;

import com.aporiadev.ahrnjic.redminetasks.view.activities.PermissionActivity;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.PERMISSION_ACTIVITY_REQ_CODE;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.appInForeground;

/**
 * Created by ahrnjic on 27/09/17.
 */

public class PermissionUtils {
    private static final String TAG = "LOG_" + PermissionUtils.class.getSimpleName();

    @TargetApi(23)
    public static boolean hasWriteExternalStoragePermission(Activity activity) {
        if (Build.VERSION.SDK_INT <= 21)
            return true;
        // Check if write external storage permission is already granted
        if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else if (appInForeground(activity)) {
            activity.startActivityForResult(new Intent(activity, PermissionActivity.class), PERMISSION_ACTIVITY_REQ_CODE);
        } else {
            // TODO: 27/09/17 Send notification about missing permission
        }
        return false;
    }

//    public void sendPermissionNotification() {
//        Uri systemSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
//
//        Notification.Builder builder = new Notification.Builder(context)
//                .setSmallIcon(R.drawable.ic_launcher_white)
//                .setLargeIcon(largeIcon)
//                .setAutoCancel(true)
//                .setSound(systemSound)
//                .setContentTitle(article.getTitle())
//                .setContentText(article.getContent());
//
//        Intent resultIntent = new Intent(context, MainActivity.class);
////        resultIntent.putExtra("Notifications", true);
//        PendingIntent resultPendingIntent =
//                PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        builder.setContentIntent(resultPendingIntent);
//
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//        notificationManager.notify(0, builder.build();
//    }
}
