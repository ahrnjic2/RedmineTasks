package com.aporiadev.ahrnjic.redminetasks.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.aporiadev.ahrnjic.redminetasks.BR;
import com.aporiadev.ahrnjic.redminetasks.model.Task;

/**
 * Created by ahrnjic on 18/09/17.
 */

public class TaskViewModel extends BaseObservable {
    private static final String TAG = "LOG_" + TaskViewModel.class.getSimpleName();

    private Task task;

    public TaskViewModel(Task task) {
        this.task = task;
    }

    @Bindable
    public String getId() {
        return String.valueOf(task.getId());
    }

    @Bindable
    public String getTitle() {
        return task.getTitle();
    }

    public void setTitle(String title) {
        task.setTitle(title);
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getUrl() {
        return task.getUrl();
    }

    public void setUrl(String url) {
        task.setUrl(url);
        notifyPropertyChanged(BR.datePublished);
    }

    @Bindable
    public int getCount() {
        return task.getPostsCount();
    }

    public void setCount(int count) {
        task.setPostsCount(count);
        notifyPropertyChanged(BR.opened);
    }
}
