package com.aporiadev.ahrnjic.redminetasks.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.model.Article;
import com.aporiadev.ahrnjic.redminetasks.view.activities.MainActivity;

import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;


/**
 * Created by ahrnjic on 19/09/17.
 */

public class NotificationUtils {
    private static final String NOTIFICATION_CHANNEL_ID = "CHANNEL_0";
    private static final String NOTIFICATION_GROUP_KEY = "GROUP_NOTIFICATIONS";

    public static void sendNotification(Context context, Article article) {
        Uri systemSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);

        Notification.Builder builder = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher_white)
                .setLargeIcon(largeIcon)
                .setAutoCancel(true)
                .setSound(systemSound)
                .setContentTitle(article.getTitle())
                .setContentText(article.getContent());

        Intent resultIntent = new Intent(context, MainActivity.class);
//        resultIntent.putExtra("Notifications", true);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }


    public static void sendNotification(Context context, List<Article> articles) {
        Uri systemSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle(context.getString(R.string.notification_new_task_updates))
                .setSmallIcon(R.drawable.ic_launcher_white)
                .setLargeIcon(largeIcon)
                .setContentText(context.getString(R.string.notification_swipe))
                .setSound(systemSound)
                .setAutoCancel(true)
                .setGroupSummary(true)
                .setGroup(NOTIFICATION_GROUP_KEY);

        Notification.InboxStyle inboxStyle = new Notification.InboxStyle();

        for (int i = 0; i < articles.size() && i < 5; i++) {
            inboxStyle.addLine(articles.get(i).getTitle());
        }


        if (articles.size() > 5)
            inboxStyle.addLine("And " + String.valueOf(articles.size() - 5) + " more.");
        else
            inboxStyle.addLine(articles.get(5).getTitle());


        inboxStyle.setBigContentTitle(context.getString(R.string.notification_new_task_updates));
        builder.setStyle(inboxStyle);

        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra("Notifications", true);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
}
