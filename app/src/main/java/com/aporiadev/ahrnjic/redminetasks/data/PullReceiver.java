package com.aporiadev.ahrnjic.redminetasks.data;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.utils.AppConstants;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_MISSED_UPDATE_NO_CONNECTION;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.appInForeground;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;

/**
 * Created by ahrnjic on 19/09/17.
 */

public class PullReceiver extends BroadcastReceiver {
    private static final String TAG = "LOG_" + PullReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = cm.getActiveNetworkInfo();
            if (activeNetInfo != null && activeNetInfo.isConnected()) {
                // If network is connected, start PullService
                Log.i(TAG, "onReceive: ");
                Intent pullIntent = new Intent(Intent.ACTION_SYNC, null, context, PullService.class);
                context.startService(pullIntent);
            } else {
                Log.i(TAG, "onReceive: no internet connection");
                if (!getPrefsBoolean(context, KEY_MISSED_UPDATE_NO_CONNECTION, false)) {
                    // If this is first update request with no connection, schedule a job to update as soon as connected
                    putPrefsBoolean(context, KEY_MISSED_UPDATE_NO_CONNECTION, true);
                    Log.i(TAG, "onReceive: no internet connection - scheduling job to update when connected");
                    JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                    jobScheduler.schedule(
                            new JobInfo.Builder(0, new ComponentName(context, JobPullService.class))
                                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                                    .build());
                }

                if (appInForeground(context)) {
                    // Notify user of network connection if app is in foreground
                    Toast.makeText(context, context.getResources().getString(R.string.toast_no_internet_connection), Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "onReceive: stop refreshing animation");
                    context.sendBroadcast(new Intent(AppConstants.INTENT_FILTER_REFRESH_COMPLETED));
                }
            }
        } catch (Exception e) {
            Log.i(TAG, "onReceive: Exception: " + e.toString());
        }
    }
}
