package com.aporiadev.ahrnjic.redminetasks.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.aporiadev.ahrnjic.redminetasks.data.PullReceiver;

/**
 * Created by ahrnjic on 19/09/17.
 */

public class AlarmUtils {
    private static final String TAG = "LOG_" + AlarmUtils.class.getSimpleName();

    /**
     * Start alarm unless one already exists.
     * @param context Needed to start intents.
     * @return true if alarm is started, false if alarm is already running.
     */
    public static boolean startAlarm(Context context) {
        Log.i(TAG, "startAlarm: starting new alarm");

        Intent intent = new Intent(context, PullReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0 , intent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_FIFTEEN_MINUTES, AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent);

        return true;
    }

}
