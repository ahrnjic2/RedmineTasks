package com.aporiadev.ahrnjic.redminetasks.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_APP_IN_FOREGROUND;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.PREFS;

/**
 * Created by ahrnjic on 22/10/17.
 */

public class SharedPrefsUtils {
    public static final String TAG = "LOG_" + SharedPrefsUtils.class.getSimpleName();

    /**
     * Return boolean value of key provided as arg, from default app preferences (PREFS, 0)
     *
     * @param context      Required to access shared preferences.
     * @param key          Preference key.
     * @param defaultValue Default value to provide.
     * @return Boolean value of preference.
     */
    public static boolean getPrefsBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, 0);

        boolean value = prefs.getBoolean(key, defaultValue);
        Log.i(TAG, "getPrefsBoolean: key = " + key + ", value = " + value);
        return value;
    }

    /**
     * Put boolean value of key provided as arg, in default app preferences (PREFS, 0)
     *
     * @param context Required to access shared preferences.
     * @param key     Preference key.
     * @param value   Value to set.
     */
    public static void putPrefsBoolean(Context context, String key, boolean value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, 0);

        Log.i(TAG, "putPrefsBoolean: key = " + key + ", value = " + value);
        prefs.edit().putBoolean(key, value).apply();
    }

    /**
     * Return String value of key provided as arg, from default app preferences (PREFS, 0)
     *
     * @param context      Required to access shared preferences.
     * @param key          Preference key.
     * @param defaultValue Default value to provide.
     * @return String value of preference.
     */
    public static String getPrefsString(Context context, String key, String defaultValue) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, 0);

        Log.i(TAG, "getPrefsString: " + key);
        return prefs.getString(key, defaultValue);
    }

    /**
     * Put String value of key provided as arg, in default app preferences (PREFS, 0)
     *
     * @param context Required to access shared preferences.
     * @param key     Preference key.
     * @param value   Value to set.
     */
    public static void putPrefsString(Context context, String key, String value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, 0);

        Log.i(TAG, "putPrefsString: key = " + key + ", value = " + value);
        prefs.edit().putString(key, value).apply();
    }

    /**
     * Return long value of key provided as arg, from default app preferences (PREFS, 0)
     *
     * @param context      Required to access shared preferences.
     * @param key          Preference key.
     * @param defaultValue Default value to provide.
     * @return long value of preference.
     */
    public static long getPrefsLong(Context context, String key, long defaultValue) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, 0);

        Log.i(TAG, "getPrefsLong: " + key);
        return prefs.getLong(key, defaultValue);
    }

    /**
     * Put long value of key provided as arg, in default app preferences (PREFS, 0)
     *
     * @param context Required to access shared preferences.
     * @param key     Preference key.
     * @param value   Value to set.
     */
    public static void putPrefsLong(Context context, String key, long value) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS, 0);

        Log.i(TAG, "putPrefsLong: key = " + key + ", value = " + value);
        prefs.edit().putLong(key, value).apply();
    }


    /**
     * Checks whether app is in foreground based on data in shared preferences.
     *
     * @param context Required to access shared preferences.
     * @return True if app is in foreground, false if app is in background of killed/dead.
     */
    public static boolean appInForeground(Context context) {
        return getPrefsBoolean(context, KEY_APP_IN_FOREGROUND, false);
    }

    public static void setAppInForeground(Context context, boolean value) {
        putPrefsBoolean(context, KEY_APP_IN_FOREGROUND, value);
    }

}
