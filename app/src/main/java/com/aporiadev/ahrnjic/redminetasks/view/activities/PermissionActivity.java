package com.aporiadev.ahrnjic.redminetasks.view.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.aporiadev.ahrnjic.redminetasks.R;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.PERMISSION_KEY_WRITE_TO_EXTERNAL_STORAGE_DONT_ASK_AGAIN;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.PERMISSION_WRITE_TO_EXTERNAL_STORAGE_REQUEST_CODE;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.setAppInForeground;

/**
 * Created by ahrnjic on 27/09/17.
 */

@TargetApi(23)
public class PermissionActivity extends AppCompatActivity {
    private static final String TAG = "LOG_" + PermissionActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getPrefsBoolean(this, PERMISSION_KEY_WRITE_TO_EXTERNAL_STORAGE_DONT_ASK_AGAIN, false)) {
            Log.i(TAG, "notifying user about missing permission - rationale not shown: 1");
            // Previously Permission Request was cancelled with 'Don't Ask Again',
            // Redirect to Settings after showing Information about why you need the permission
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.permission_write_storage);
            builder.setMessage(R.string.permission_previously_denied);
            builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.i(TAG, "onClick: redirecting to settings");
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 101);
                    Toast.makeText(PermissionActivity.this, R.string.permission_manually, Toast.LENGTH_LONG).show();
                    dialog.cancel();
                }
            });
            builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        } else {
            //Show Information about why you need the permission
            Log.i(TAG, "requesting permission with rationale: 2");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.permission_write_storage);
            builder.setMessage(R.string.permission_intro);
            builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_TO_EXTERNAL_STORAGE_REQUEST_CODE);
                    dialog.cancel();
                }
            });
            builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume: ");
        setAppInForeground(this, true);
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_WRITE_TO_EXTERNAL_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted
                // TODO: 27/09/17 Pull all files with size=-1
                Log.i(TAG, "onRequestPermissionsResult: permission granted");
                setResult(RESULT_OK);
                finish();
            } else if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Permission Request was cancelled with 'Don't Ask Again'
                putPrefsBoolean(this, PERMISSION_KEY_WRITE_TO_EXTERNAL_STORAGE_DONT_ASK_AGAIN, true);
                Log.i(TAG, "onRequestPermissionsResult: permission denied - won't ask again");
            } else {
                // Permission Request was cancelled but can be asked for again
                Toast.makeText(this, R.string.permission_intro, Toast.LENGTH_SHORT).show();
                Log.i(TAG, "onRequestPermissionsResult: permission denied - will ask next time");
            }
            setResult(RESULT_CANCELED);
            finish();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
