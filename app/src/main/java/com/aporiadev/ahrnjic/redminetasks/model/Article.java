package com.aporiadev.ahrnjic.redminetasks.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ahrnjic on 17/09/17.
 */

public class Article extends RealmObject {
    private int id;
    private Task task;
    private String title;
    private String content;
    private RealmList<Attachment> attachments;
    private long datePublished;
    private boolean opened;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAttachments(RealmList<Attachment> attachments) {
        this.attachments = attachments;
    }

    public RealmList<Attachment> getAttachments() {
        return attachments;
    }

    public long getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(long datePublished) {
        this.datePublished = datePublished;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

}
