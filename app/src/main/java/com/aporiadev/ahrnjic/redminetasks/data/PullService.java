package com.aporiadev.ahrnjic.redminetasks.data;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.util.Log;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.model.Article;
import com.aporiadev.ahrnjic.redminetasks.model.Attachment;
import com.aporiadev.ahrnjic.redminetasks.model.Task;
import com.aporiadev.ahrnjic.redminetasks.utils.AppConstants;
import com.aporiadev.ahrnjic.redminetasks.utils.ServiceToast;
import com.einmalfel.earl.EarlParser;
import com.einmalfel.earl.Feed;
import com.einmalfel.earl.Item;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.APP_DEFAULT_SAVE_PATH;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.ATTACHMENTS_BASE_URL;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_BASE_URL;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_CLEAR_ANIMATION_ON_RESUME;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_LAST_UPDATED;
import static com.aporiadev.ahrnjic.redminetasks.utils.NotificationUtils.sendNotification;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.appInForeground;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsString;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsLong;

/**
 * Created by ahrnjic on 20/09/17.
 */

public class PullService extends IntentService {
    private static final String TAG = "LOG_" + PullService.class.getSimpleName();

    private List<Article> newArticles;

    private Handler handler;

    /**
     * A constructor is required, and must call the super IntentService(String)
     * constructor with a name for the worker thread.
     */
    public PullService() {
        super("PullService");
        handler = new Handler();
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */

    @Override
    protected void onHandleIntent(Intent intent) {
        final String url = getPrefsString(this, KEY_BASE_URL, "");
        // To know whether we should start/stop animation and notify ArticleListFragment's onResume to clear any previous animations
        boolean animationStarted = false;

        if (url.equals("")) {
            handler.post(new ServiceToast(this, getString(R.string.toast_incorrect_url)));
            sendBroadcast(new Intent(AppConstants.INTENT_FILTER_REFRESH_COMPLETED));
            return;
        }

        // Start loading animation on SwipeRefreshLayout if app is in foreground
        if (appInForeground(this)) {
            Log.i(TAG, "onHandleIntent: start refreshing animation");
            sendBroadcast(new Intent(AppConstants.INTENT_FILTER_REFRESH_STARTED));
            animationStarted = true;
        }

        newArticles = new ArrayList<>();
        // Init is needed so that service can work without main UI thread
        Realm.init(this.getApplicationContext());
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    InputStream tasksInputStream = new URL(url).openConnection().getInputStream();
                    Feed tasksFeed = EarlParser.parseOrThrow(tasksInputStream, 0);
                    Calendar c = Calendar.getInstance();
                    Log.i(TAG, "Processing feed: " + tasksFeed.getTitle());
                    int id;
                    for (Item item : tasksFeed.getItems()) {
                        id = Integer.parseInt(item.getId().substring(item.getId().lastIndexOf("/") + 1));
                        // Insert new Task
                        if (realm.where(Task.class).equalTo("id", id).findAll().size() == 0) {
                            Log.i(TAG, "execute: Inserting Task:" + item.getTitle());
                            // Add a person
                            Task task = realm.createObject(Task.class);
                            task.setId(id);
                            task.setBaseUrl(item.getLink());
                            task.setUrl(item.getLink() + ".atom?key=c88c0db616332825923c838ef1c5faf8904430a0");
                            task.setTitle(item.getTitle());
                            task.setPostsCount(0);
                            c.setTime(item.getPublicationDate());
                            task.setLastUpdated(c.getTimeInMillis());
                            Article article = realm.createObject(Article.class);
                            article.setId(id);
                            article.setTask(task);
                            article.setTitle(task.getTitle());
                            String content = Html.fromHtml(item.getDescription()).toString().replaceAll("\n\n", "\n");
                            article.setContent(content);
                            article.setAttachments(extractFiles(item.getDescription()));
                            article.setDatePublished(c.getTimeInMillis());
                            article.setOpened(false);
                            newArticles.add(realm.copyFromRealm(article));
                        } else {
                            Log.i(TAG, "Already exists:" + item.getId());
                        }
                    }

                    // Pull every Task and parse its updates
                    InputStream updatesInputStream;
                    Feed articlesFeed;
                    for (Task task : realm.where(Task.class).findAll()) {
                        updatesInputStream = new URL(task.getUrl()).openConnection().getInputStream();
                        articlesFeed = EarlParser.parseOrThrow(updatesInputStream, 0);
                        Log.i(TAG, "Processing articlesFeed: " + task.getTitle());
                        Log.i(TAG, "articlesFeed count: " + articlesFeed.getItems().size());
                        if (task.getPostsCount() != articlesFeed.getItems().size()) {
                            for (Item item : articlesFeed.getItems()) {
                                id = Integer.parseInt(item.getId().substring(item.getId().lastIndexOf("id=") + 3));
                                // Insert new task update
                                if (realm.where(Article.class).equalTo("id", id).findAll().size() == 0) {
                                    Log.i(TAG, "execute: Inserting Article:" + id + " for task " + task.getId());
                                    // Add a person
                                    Article article = realm.createObject(Article.class);
                                    article.setId(id);
                                    article.setTask(task);
                                    article.setTitle(task.getTitle());
                                    String content = Html.fromHtml(item.getDescription()).toString().replaceAll("\n\n", "\n");
                                    article.setContent(content);
                                    article.setAttachments(extractFiles(item.getDescription()));
                                    c.setTime(item.getPublicationDate());
                                    article.setDatePublished(c.getTimeInMillis());
                                    article.setOpened(false);
                                    // Update tasks last updated time
                                    if (c.getTimeInMillis() > task.getLastUpdated()) {
                                        Log.i(TAG, "execute: updating task lastUpdated");
                                        task.setLastUpdated(c.getTimeInMillis());
                                    }
                                    newArticles.add(realm.copyFromRealm(article));
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.i(TAG, "execute: Exception: " + e.toString());
                }
            }
        });

        realm.close();

        // Stop loading animation on SwipeRefreshLayout if app is in foreground
        if (animationStarted) {
            if (appInForeground(this)) {
                Log.i(TAG, "onHandleIntent: stop refreshing animation");
                sendBroadcast(new Intent(AppConstants.INTENT_FILTER_REFRESH_COMPLETED));
            } else {
                putPrefsBoolean(this, KEY_CLEAR_ANIMATION_ON_RESUME, true);
            }
        }

        // Send broadcast for jobFinished() if Service was started from job
        if (intent.getExtras() != null && intent.getExtras().getBoolean("JobPullService")) {
            Log.i(TAG, "onHandleIntent: finish job");
            sendBroadcast(new Intent(AppConstants.INTENT_FILTER_JOB_PULL_SERVICE_COMPLETED));
        }

        checkAndSendNotifications();
    }

    /**
     * Check if app is in foreground and if there are new articles added.
     * If so, call single or group notification.
     */
    protected void checkAndSendNotifications() {
        Log.i(TAG, "checkAndSendNotifications: new articles count: " + newArticles.size());

        if (!appInForeground(this)) {
            if (newArticles.size() == 1)
                sendNotification(this, newArticles.get(0));
            else if (newArticles.size() > 1)
                sendNotification(this, newArticles);
        }

        Log.i(TAG, "checkAndSendNotifications: updating KEY_LAST_UPDATED");
        putPrefsLong(this, KEY_LAST_UPDATED, System.currentTimeMillis());
    }

    /**
     * Extract files from content string into a list of files. Files will NOT be downloaded.
     *
     * @param content Content string to extract (parse) files from.
     * @return List of Attachment objects compatible for Realm.
     */
    private RealmList<Attachment> extractFiles(final String content) {
        Realm realm = Realm.getDefaultInstance();
        RealmList<Attachment> attachments = new RealmList<>();
        String tmp = content;
        int count = realm.where(Attachment.class).findAll().size();
        while (tmp.contains("<a href") && tmp.contains("</a>")) {

            if (tmp.indexOf("<a href") > tmp.indexOf("</a>")) {
                break;
            }
            // Get html in <a> tag
            String f = tmp.substring(tmp.indexOf("<a href=\"") + 9, tmp.indexOf("</a>"));

            String fileUrl = ATTACHMENTS_BASE_URL + f.substring(0, f.indexOf("\""));
            String fileName = f.substring(f.indexOf(">") + 1).replaceAll(" ", "_");
            String fileExt = f.substring(f.indexOf("."), f.indexOf("\""));

            tmp = tmp.substring(tmp.indexOf("</a>") + 4);
            Attachment attachment = realm.createObject(Attachment.class);
            attachment.setId(count++);
            attachment.setName(fileName);
            attachment.setUrl(fileUrl);
            attachment.setType(fileExt);
            attachment.setPath(APP_DEFAULT_SAVE_PATH + fileName);
            attachment.setSize(-1);
            attachments.add(attachment);

            Log.i(TAG, "extractFiles: fileName: " + attachment.getName());
            Log.i(TAG, "extractFiles: fileUrl: " + attachment.getUrl());
            Log.i(TAG, "extractFiles: fileExt: " + attachment.getType());
            Log.i(TAG, "extractFiles: filePath: " + attachment.getPath());
            Log.i(TAG, "extractFiles: fileSize: " + attachment.getSize());
        }
        Log.i(TAG, "extractFiles: Article contains : " + attachments.size() + " attachments");
        realm.close();
        return attachments;
    }

}