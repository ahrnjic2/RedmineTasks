package com.aporiadev.ahrnjic.redminetasks.view.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.view.fragments.ArticleListFragment;
import com.aporiadev.ahrnjic.redminetasks.view.fragments.AuthDialogFragment;
import com.aporiadev.ahrnjic.redminetasks.view.fragments.DetailsFragment;
import com.aporiadev.ahrnjic.redminetasks.view.fragments.InitDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

import static com.aporiadev.ahrnjic.redminetasks.utils.AlarmUtils.startAlarm;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.INTENT_FILTER_PULL_RECEIVER;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_ALARM_EXISTS;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_APP_IN_FOREGROUND;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_BASE_URL;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_FIRST_LAUNCH;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_LAST_UPDATED;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.PREFS;
import static com.aporiadev.ahrnjic.redminetasks.utils.Common.dateToFormattedString;
import static com.aporiadev.ahrnjic.redminetasks.utils.RealmUtils.markAllAsRead;
import static com.aporiadev.ahrnjic.redminetasks.utils.RealmUtils.resetDatabase;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.appInForeground;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsBoolean;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsLong;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.getPrefsString;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;

public class MainActivity extends AppCompatActivity implements InitDialogFragment.ICallback, AuthDialogFragment.ICallback {

    private static final String TAG = "LOG_" + MainActivity.class.getSimpleName();
    @BindView(R.id.time_updated)
    TextView tv;

    SharedPreferences.OnSharedPreferenceChangeListener listener;
    public static boolean isFiltered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);
        ButterKnife.bind(this);

//        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        binding.collapsingToolbar.setTvm(new ToolbarViewModel(this));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getPrefsBoolean(MainActivity.this, KEY_FIRST_LAUNCH, true)
                || getPrefsString(MainActivity.this, KEY_BASE_URL, "").equals("")) {
            InitDialogFragment initDialogFragment = new InitDialogFragment();
            initDialogFragment.show(getFragmentManager(), "SetBaseUrl");
        }
        Log.i(TAG, "onCreate: " + getPackageName());

        FragmentManager fragmentManager = getFragmentManager();
        ArticleListFragment articleListFragment = (ArticleListFragment) fragmentManager.findFragmentByTag("ArticleList");
        isFiltered = false;

        if (articleListFragment == null) {
            articleListFragment = new ArticleListFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.contentFrame, articleListFragment, "ArticleList")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause: ");
        putPrefsBoolean(MainActivity.this, KEY_APP_IN_FOREGROUND, false);
        getSharedPreferences(PREFS, 0).unregisterOnSharedPreferenceChangeListener(listener);
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume: ");
        putPrefsBoolean(MainActivity.this, KEY_APP_IN_FOREGROUND, true);
        registerOnUpdatedListener();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            Log.i(TAG, "onBackPressed: getBackStackEntryCount() > 0");
            fragmentManager.popBackStack();
            setBackButtonVisible(false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//         Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i(TAG, "onOptionsItemSelected: icon back");
                onBackPressed();
                return true;
            case R.id.reset_database:
                boolean re = resetDatabase();
                // Go back to rv fragment on delete - only if on details fragment
                FragmentManager fragmentManager = getFragmentManager();
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    fragmentManager.popBackStack();
                    setBackButtonVisible(false);
                }
                return re;
            case R.id.mark_all_as_read:
                return markAllAsRead();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void registerOnUpdatedListener() {
        // Set time on first run
        long lastUpdated = getPrefsLong(MainActivity.this, KEY_LAST_UPDATED, 0);
        if (lastUpdated != 0) {
            tv.setText(getResources().getString(R.string.last_updated).concat(" ").concat(dateToFormattedString(lastUpdated)));
        }
        listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                if (key.equals(KEY_LAST_UPDATED) && appInForeground(MainActivity.this)) {
                    tv.setText(getResources().getString(R.string.last_updated).concat(" ").concat(dateToFormattedString(prefs.getLong(KEY_LAST_UPDATED, 0))));
                    Log.i(TAG, "onSharedPreferenceChanged: KEY_LAST_UPDATED - updating textView");
                }
            }
        };
        getSharedPreferences(PREFS, 0).registerOnSharedPreferenceChangeListener(listener);
    }

    private void setBackButtonVisible(boolean value) {
        if (getSupportActionBar() != null) {
            Log.i(TAG, "setBackButtonVisible: ");
            getSupportActionBar().setDisplayShowHomeEnabled(value);
            getSupportActionBar().setDisplayHomeAsUpEnabled(value);
        }
    }

    // Interfaces
    @Override
    public void pullAndStartAlarm() {
        sendBroadcast(new Intent(INTENT_FILTER_PULL_RECEIVER));
        startAlarm(this);
        putPrefsBoolean(MainActivity.this, KEY_ALARM_EXISTS, true);
    }

    @Override
    public void proceedDownload() {
        Log.i(TAG, "proceedDownload: ");
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.findFragmentByTag("ArticleDetails") != null) {
            DetailsFragment df = (DetailsFragment) fragmentManager.findFragmentByTag("ArticleDetails");
            df.proceedDownload();
        }
    }
}


