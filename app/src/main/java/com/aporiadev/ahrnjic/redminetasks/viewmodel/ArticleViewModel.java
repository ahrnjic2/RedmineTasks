package com.aporiadev.ahrnjic.redminetasks.viewmodel;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.aporiadev.ahrnjic.redminetasks.BR;
import com.aporiadev.ahrnjic.redminetasks.R;
import com.aporiadev.ahrnjic.redminetasks.model.Article;
import com.aporiadev.ahrnjic.redminetasks.view.fragments.DetailsFragment;

import static com.aporiadev.ahrnjic.redminetasks.utils.Common.dateToFormattedString;

/**
 * Created by ahrnjic on 18/09/17.
 */

public class ArticleViewModel extends BaseObservable {
    private static final String TAG = "LOG_" + ArticleViewModel.class.getSimpleName();

    private Article article;

    public ArticleViewModel(Article article) {
        this.article = article;
    }

    @Bindable
    public String getId() {
        return String.valueOf(article.getId());
    }

    @Bindable
    public String getTitle() {
        return article.getTitle();
    }

    public void setTitle(String title) {
        article.setTitle(title);
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getContent() {
        return article.getContent();
    }

    public void setContent(String content) {
        article.setContent(content);
        notifyPropertyChanged(BR.content);
    }

    @Bindable
    public String getDatePublished() {
        return dateToFormattedString(article.getDatePublished());
    }

    public void setDatePublished(long datePublished) {
        article.setDatePublished(datePublished);
        notifyPropertyChanged(BR.datePublished);
    }

    @Bindable
    public boolean getOpened() {
        return article.isOpened();
    }

    public void setOpened(boolean opened) {
        article.setOpened(opened);
        notifyPropertyChanged(BR.opened);
    }

    @Bindable
    public String getTaskLink() {
        return article.getTask().getBaseUrl();
    }

    @Bindable
    public int getTaskFilesCount() {
        return article.getAttachments().size();
    }

    // Methods
    public void openArticleDetails(View view) {
        Log.i(TAG, "onClick: " + article.getId());

        FragmentManager fragmentManager = ((Activity) view.getContext()).getFragmentManager();

        DetailsFragment detailsFragment = (DetailsFragment) fragmentManager.findFragmentByTag("ArticleDetails");
//        isFiltered = false;

        if (detailsFragment == null) {
            detailsFragment = new DetailsFragment();

            Bundle args = new Bundle();
            args.putInt("ID", article.getId());
            detailsFragment.setArguments(args);

            fragmentManager.beginTransaction()
                    .replace(R.id.contentFrame, detailsFragment, "ArticleDetails")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void startBrowserIntent(View view) {
        Intent openInBrowser = new Intent(Intent.ACTION_VIEW);
        openInBrowser.setData(Uri.parse(getTaskLink()));
        openInBrowser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        view.getContext().startActivity(openInBrowser);

    }

}
