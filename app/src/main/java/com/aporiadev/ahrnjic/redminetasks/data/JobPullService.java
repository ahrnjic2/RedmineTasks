package com.aporiadev.ahrnjic.redminetasks.data;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.INTENT_FILTER_JOB_PULL_SERVICE_COMPLETED;
import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.KEY_MISSED_UPDATE_NO_CONNECTION;
import static com.aporiadev.ahrnjic.redminetasks.utils.SharedPrefsUtils.putPrefsBoolean;

/**
 * Created by ahrnjic on 22/10/17.
 */

public class JobPullService extends JobService {
    private static final String TAG = "LOG_" + JobPullService.class.getSimpleName();

    BroadcastReceiver broadcastReceiver;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        Log.i(TAG, "onStartJob: id = " + jobParameters.getJobId());

        // Register receiver for jobFinished()
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, "onReceive: received broadcast - finishing job id = " + jobParameters.getJobId());
                jobFinished(jobParameters, false);
                unregisterReceiver(broadcastReceiver);
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(INTENT_FILTER_JOB_PULL_SERVICE_COMPLETED));

        // pull
        Intent pullIntent = new Intent(Intent.ACTION_SYNC, null, this, PullService.class);
        pullIntent.putExtra("JobPullService", true);
        startService(pullIntent);

        // clear SharedPreferences' key for missed update
        putPrefsBoolean(this, KEY_MISSED_UPDATE_NO_CONNECTION, false);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.i(TAG, "onStopJob: id = " + jobParameters.getJobId());
        unregisterReceiver(broadcastReceiver);
        return false;
    }
}
