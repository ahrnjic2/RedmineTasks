package com.aporiadev.ahrnjic.redminetasks.utils;

import android.util.Log;

import com.aporiadev.ahrnjic.redminetasks.model.Article;

import io.realm.Realm;
import io.realm.exceptions.RealmException;

/**
 * Created by ahrnjic on 21/10/17.
 */

public class RealmUtils {
    private static final String TAG = "LOG_" + RealmUtils.class.getSimpleName();

    public static boolean resetDatabase() {
        Log.i(TAG, "resetDatabase: ");
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.deleteAll();
                }
            });
        } catch (RealmException e) {
            Log.i(TAG, "markAllAsRead: RealmException: " + e.toString());
            return false;
        } catch (Exception e) {
            Log.i(TAG, "markAllAsRead: Exception: " + e.toString());
            return false;
        }
        return true;
    }

    public static boolean markAllAsRead() {
        Log.i(TAG, "markAllAsRead: ");
        try (Realm realm = Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (Article a : realm.where(Article.class).findAll()) {
                        if (!a.isOpened()) {
                            a.setOpened(true);
                        }
                    }
                }
            });
        } catch (RealmException e) {
            Log.i(TAG, "markAllAsRead: RealmException: " + e.toString());
            return false;
        } catch (Exception e) {
            Log.i(TAG, "markAllAsRead: Exception: " + e.toString());
            return false;
        }
        return true;
    }
}
