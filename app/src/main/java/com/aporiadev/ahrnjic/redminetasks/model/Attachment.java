package com.aporiadev.ahrnjic.redminetasks.model;

import io.realm.RealmObject;

/**
 * Created by ahrnjic on 26/09/17.
 */

public class Attachment extends RealmObject {
    private int id;
    private String name;
    private String url;
    private String type;
    /**
     * Attachment path on system.
     */
    private String path;
    /**
     * Size in bytes
     */
    private long size;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return Size of file in bytes.
     */
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    /**
     * @return Attachment path on system.
     */
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
