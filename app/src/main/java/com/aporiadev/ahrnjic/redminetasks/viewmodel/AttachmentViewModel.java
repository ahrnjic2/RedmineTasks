package com.aporiadev.ahrnjic.redminetasks.viewmodel;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.aporiadev.ahrnjic.redminetasks.model.Attachment;

import java.io.File;

import static com.aporiadev.ahrnjic.redminetasks.utils.AppConstants.GENERIC_FILE_PROVIDER_AUTHORITIES;

/**
 * Created by ahrnjic on 26/09/17.
 */

public class AttachmentViewModel extends BaseObservable {
    private static final String TAG = "LOG_" + AttachmentViewModel.class.getSimpleName();
    private Attachment attachment;

    public AttachmentViewModel(Attachment attachment) {
        Log.i(TAG, "AttachmentViewModel: ");
        this.attachment = attachment;
    }

    @Bindable
    public String getName() {
        return attachment.getName();
    }

    @Bindable
    public String getType() {
        return attachment.getType();
    }

    @Bindable
    public long getSize() {
        return attachment.getSize();
    }

    @Bindable
    public String getPath() {
        return attachment.getPath();
    }

    public void openAttachment(View view) {
        Log.i(TAG, "onClick: " + attachment.getName());
        Log.i(TAG, "onClick: path: " + attachment.getPath());
        File file = new File(attachment.getPath());
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
        String mimeType = myMime.getMimeTypeFromExtension(attachment.getType().substring(1));
        Uri uri = FileProvider.getUriForFile(view.getContext(), GENERIC_FILE_PROVIDER_AUTHORITIES, file);
        Log.i(TAG, "onClick: ur: " + uri);
        newIntent.setDataAndType(uri, mimeType);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            view.getContext().startActivity(newIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(view.getContext(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
        }
    }
}
